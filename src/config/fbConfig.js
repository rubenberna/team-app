import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

// Initializing Firebase Database
var config = {
  apiKey: process.env.REACT_APP_FIREBASE_KEY,
  authDomain: "drkingmanage.firebaseapp.com",
  databaseURL: "https://drkingmanage.firebaseio.com",
  projectId: "drkingmanage",
  storageBucket: "drkingmanage.appspot.com",
  messagingSenderId: "15016643319",
  appId: "1:15016643319:web:a95b63d7f5565226"
};
firebase.initializeApp(config);
firebase.firestore().settings({ timestampsInSnapshots: true });

export default firebase
